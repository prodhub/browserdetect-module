# BrowserDetect

Browser detect module  
http://browserdetect.module.frontend.production.adwatch.ru

## Install

```sh
$ npm install --save @adwatch/browserdetect
```

## Usage

```js
import BrowserDetect from '@adwatch/browserdetect';		// for es6

var BrowserDetect = require('@adwatch/browserdetect/build');	// for es5

let browserDetect = new BrowserDetect(options);
```


## Get started

```js

let browserDetect = new BrowserDetect(options);

```


## API

#### Options	


| Name                       |   Type   |  Description |
| :------------------------- | :--------- | :------ |
| `timeOutLocalStorageName ` | `number  ` | Variable timeout name for localStorage. Default - `0`. |
| `check                   ` | `array   ` | Browser verification options*. |
| `onInit                  ` | `function` | Inited browsercheck callback. |
| `onCheckFailed           ` | `function` | Checked old browser callback. |


\* Browser verification options:

Array - `[name, platform, version, device], [name, platform, version, device],...`

| Value         |   Type     |  Description |
| :------------ | :--------- | :------ |
| `name `       | `string  ` | Browser name (_msie, edge, safari, blackberry, yandex, playbook, opera, opera mini, android, kindle, silk, firefox, chrome_) - **required**. |
| `version ` 	| `string`   | Maximum browser version - **optional**. |
| `platform `	| `string  ` | Platform (_ipod, ipad, iphone, kindle, silk, android, windows phone, win, mac, linux, cros, playbook, blackberry_) - **optional**. |
| `device `  	| `string`   | Device (_desktop, mobile_) - **optional**. |

```js
/**
* 	Default:
* 	
*	['edge', '14', false, 'desktop'],
*	['msie', '11', false, 'desktop'],
*	['chrome', '49', 'win', 'desktop'],
*	['firefox', '52'],
*	['opera', '47'],
*	['safari', false, 'win'],
*	['safari', '10', 'mac']
* 	
*/

```




### Callbacks option arguments

onInit arguments: `({ browser data})`

onCheckFailed arguments: `({ browser data })`


#### Callbacks return data:

| Key                        |   Type     |  Description |
| :------------------------- | :--------- | :------ |
| `browserName             ` | `string  ` | Browser name |
| `browserVersion          ` | `number  ` | Browser version major. |
| `browserVersionFull      ` | `string`   | Browser full version |
| `checkFailed             ` | `boolean`  | Browser compliance with the specified options |
| `devicePlatform          ` | `string`   | Device platform |
| `deviceType              ` | `string`   | Device type (_desktop or mobile_) |
| `timeout                 ` | `number`   | Time of absence of verification (_in ms_) |
| `webkit                  ` | `boolean`  | Webkit - type check |




## Methods	

#### browserDetect.setTimeOut(hours);
Setting the time in hours for absence of verification


### browserDetect.getTimeOut(type)
Return actual timeout in ms 

type (default returned in milliseconds);  
`s` - returned in seconds;  
`m` - returned in minutes;  
`h` - returned in hours;  





## License

MIT © 